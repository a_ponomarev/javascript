/*
 GAME RULES:
 
 - The game has 2 players, playing in rounds.
 * In each turn, a player may roll the dice many times.
 
 - Each result gets added to current ROUND score
 * If the player rolls a 1, all the ROUND score gets lost. After that, it's the next player's turn.
 
 - The player can choose to 'Hold'
 * Current ROUND score gets added to the GLOBAL score. 
 * After that, it's the next player's turn
 
 - The first player to reach 20 points on GLOBAL score wins the game
 */
var init = function ()
{
    scores = [0, 0];
    currentPlayer = 0;
    roundScore = 0;

    //initialize interface
    document.getElementById("score-0").textContent = 0;
    document.getElementById("score-1").textContent = 0;
    document.getElementById("current-0").textContent = 0;
    document.getElementById("current-1").textContent = 0;
    gameRunning = true;

    //making player 1 active
    document.querySelector(".player-0-panel").classList.remove("active");
    document.querySelector(".player-1-panel").classList.remove("active");
    document.querySelector(".player-0-panel").classList.add("active");
    //reseting winner class
    document.querySelector(".player-0-panel").classList.remove("winner");
    document.querySelector(".player-1-panel").classList.remove("winner");
    //reseting name
    document.getElementById("name-0").textContent = "PLAYER 1";
    document.getElementById("name-1").textContent = "PLAYER 2";
    //remove dice
    document.querySelector(".dice").style.visibility = "hidden";

    // change the players names

    document.getElementById("name-0").onclick = nameChange;
    document.getElementById("name-1").onclick = nameChange;

    document.getElementById("winscorelimit").value = winningPoint;
    document.getElementById("winscorelimit").onkeyup = function (e) {

        if (e.keyCode == 13) {
            changePoints();
            alert("The score is changed");
            document.getElementById("winscorelimit").value = winningPoint;
            init();
        }

    };

};

function nameChange(e) {
    
    let name = prompt("Enter new name (blank or digits not allowed)", document.getElementById(e.target.id).innerHTML);
    if (name!=null && isNaN(name))
        document.getElementById(e.target.id).innerHTML = name;
    else
        document.getElementById(e.target.id).innerHTML = document.getElementById(e.target.id).innerHTML
}

function changePoints() {

    winningPoint = document.getElementById("winscorelimit").value;

}
var dice, scores, roundScore, currentPlayer, winningPoint, gameRunning;
winningPoint = 30;

//initilizing the game
init();
let tmp = 0;
let tmp2 = 0;
var rollDice = function ()
{
    
    if (gameRunning)
    {
        document.querySelector(".dice").style.visibility = "visible";
        dice = Math.floor(Math.random() * 6 + 1);
        tmp2 = tmp;
        tmp = dice;
        
        var diceImg = document.querySelector(".dice");
        diceImg.src = "dice-" + dice + ".png";
        if (dice !== 1)
        {
            if(tmp2==6 && tmp==6) { 
                alert("2 sixes in a row. All your global results dump");
                document.getElementById("score-" + currentPlayer).textContent = 0;
                swapPlayer();
                return;
                
            }
            roundScore = dice + roundScore;
            document.getElementById("current-" + currentPlayer).textContent = roundScore;
        } else {
            swapPlayer();
        }
    }
};
document.querySelector(".btn-roll").addEventListener("click", rollDice);
//Hold function
var hold = function ()
{
    if (gameRunning)
    {
        scores[currentPlayer] += roundScore;
        document.getElementById("score-" + currentPlayer).textContent = scores[currentPlayer];
        //Winner Check
        if (scores[currentPlayer] >= winningPoint)
        {
            document.querySelector(".player-" + currentPlayer + "-panel").classList.add("Winner");
            let tmpWinnerName = document.getElementById("name-" + currentPlayer).textContent;
            document.getElementById("name-" + currentPlayer).textContent = "WINNER !!"
            let tmpDate = new Date();
            document.querySelector(".scoretable").innerHTML += "<br>"+tmpDate.toUTCString()+" - "+"<b>Winner</b> is "+tmpWinnerName+"; <em>Score</em> - "+scores[currentPlayer]+"<br>"+"Loser is "+document.getElementById("name-"+((currentPlayer==0)?1:0)).textContent+"; <em>Score</em> - "+scores[(currentPlayer==0)?1:0]+"<br>";
            gameRunning = false;
            document.querySelector(".dice").style.visibility = "hidden";
        } else
            swapPlayer();
    }

};
document.querySelector(".btn-hold").addEventListener("click", hold);

var swapPlayer = function ()
{
    tmp=0;
    tmp2=0;
    document.getElementById("current-" + currentPlayer).textContent = 0;
    roundScore = 0;
    currentPlayer = (currentPlayer === 0) ? 1 : 0;
    document.querySelector(".player-0-panel").classList.toggle("active");
    document.querySelector(".player-1-panel").classList.toggle("active");
};
document.querySelector(".btn-new").addEventListener("click", init);
