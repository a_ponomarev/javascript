let scorePl = 0;
let scoreComp = 0;
let plCounter = 0;
let compCounter = 0;
let plScore = $(".pl-score");
let compScore = $(".comp-score");
let timer;
let animInt;
let leftpos = 0;
let z = 0;
let angle = -10;
let rounds = 1;
var mass = [];
let cardNum = 52;
let coinStart = 40;
let coinEnd = 519;
let zIndCoin = 1;
let passBtn = $("#passButton");

objPositions();
cardAndCoinsAppearance();

// Rules Dialog with the information

$(".rules-button").click(
        function (e) {
            e.stopPropagation();
            $(".rules").animate({"opacity": "1", "left": "230px"}, 1000);
            $('.rulesImg1').rotate({animateTo: -10, duration: 500});
            $('.rulesImg2').rotate({animateTo: 10, duration: 500});
            let rulesAnimationTimer = setInterval(function () {
                $('.rulesImg1').rotate({animateTo: -14, duration: 400});
                $('.rulesImg2').rotate({animateTo: 14, duration: 400});
                setTimeout(togleRotateB, 600);
                function togleRotateB() {
                    $('.rulesImg1').rotate({animateTo: -10, duration: 400});
                    $('.rulesImg2').rotate({animateTo: 10, duration: 400});
                }

            }, 5000)
            $(".container").addClass("blur");
            $(".container").css('height', $(window).height());
            $(".container").on("click", function () {

                $(".container").removeClass("blur");
                $(".rules").animate({"left": "-1500px", "opacity": "0"}, 1000);
                $(".container").off("click");
                clearInterval(rulesAnimationTimer);
            });


        });

function objPositions() {
//    $(".game-coins").css('left', 500 + 'px');
//    $(".final").css({'left': ($(window).width() / 2) - 260 + 'px', 'top': ($(window).height() / 2) - 120 + 'px'});
//    $(".rules").css({'left': (($(window).width() / 2) - 300) + "px"});
//    $(".rules-button").css({'left': (($(window).width() / 2)) + "px"});
//    $(".btns").css({'left': (($(window).width()) - 440) + 'px', 'top': $(window).height() - 50 + 'px'});
    $(".rules-button").toggle();
    passBtn.toggle();
    $(".game-coins").toggle();
    passBtn.attr("disabled", "disabled");
}

function cardAndCoinsAppearance() {
    for (var i = 1; i <= 52; i++) {
        $(".game-card").first().clone().attr('id', '' + i).appendTo('.play-area').addClass("rotate").css('width', '200');

    }

    for (var i = 1; i <= 10; i++) {
        $(".game-coins").first().clone().attr('class', 'game-coins gscore coin-' + i).appendTo('.play-area').css("position", "absolute");

    }

// Random non-repating numeric array creation

    for (var t = 1; t <= 52; t++) {
        mass[t] = rnd();

        for (var j = 1; j < t; j++) {
            if (j == t) {
            } else if (mass[j] == mass[t]) {
                t--;
                break;
            }
        }
    }

// end                

// All the cards and coins slide down from the top 

    for (var x = 1; x <= 52; x++) {
        var $this = $('#' + x);
        $this.animate({
            top: '+=' + ((x * 6) + 45) + 'px'}, 500);
    }

    for (var x = 1; x <= 10; x++) {
        var $this = $('.coin-' + x);
        $this.animate({
            top: '+=' + (x * 16) + 'px'}, 500);
    }
    $(".game-coins").first().hide(0);
    $(".game-card").first().hide(0);
    $(".game-card").first().removeClass("game-card");

// end

// Putting all the cards in order on the right side of the screen

    for (var x = 1; x <= 52; x++) {
        var $this = $('#' + mass[x]);
        $this.delay(x * 60).animate({
            top: '' + ((x * 0.2) + 191) + 'px',
            left: '' + (600 + x) + 'px',
            'z-index': '' + x
        }, 700);

    }

// end

    animInt = setInterval(
            cardsReshuffle
            , 8000);

    setTimeout(function () {
        returnClickToDeck();
        cardHovering();
    }, 4000);

    $('#' + mass[cardNum]).addClass("active");
}

function returnClickToDeck() {
    $(".game-card").on("click", userGo);
}

function userGo() {
    passBtn.attr("disabled", "disabled");

    let id = mass[cardNum];
    let $this = $('#' + mass[cardNum]);
    if ($this.hasClass('active')) {
        $this.removeClass("rotate");
        $this.rotate({animateTo: 0, duration: 800});
        leftpos = (leftpos == 0) ? leftpos += 4 : leftpos += 5;

        $(".game-card").off("click");

        $this.animate({"left": leftpos + "%", "top": "380px", 'width': '159px', 'height': '228px'}, 1000);

        setTimeout(function () {
            $this.rotate({animateTo: angle += 4, duration: 800});
            $this.css("z-index", z++);
            $this.children('img').prop({"src": "cards/" + id + ".png"});
            $this.removeClass("game-card active").addClass("laid");
            $('#' + mass[cardNum]).addClass("active");
            scorePl += getScore(id);
            plScore.text(scorePl);
            setTimeout(getLocalScore, 100);
            returnClickToDeck();
            passBtn.prop("disabled", false);
        }, 1000);
        cardNum--;
    }
}

function cardsReshuffle() {
    $(".game-card").off("click");
    for (var i = 1; i <= 52; i++) {
        if ($('#' + mass[i]).hasClass('rotate')) {
            $('#' + mass[i]).animate({'left': '+=35px', 'top': '-=40px'}, 400);
        }
    }

    setTimeout(toggleRotate, 500);
    function toggleRotate() {
        for (var i = 1; i <= 52; i++) {
            if ($('#' + mass[i]).hasClass('rotate')) {
                $('#' + mass[i]).delay(i * 40).animate({'left': '-=35px', 'top': '+=40px'}, 100);
            }
        }
        setTimeout(function () {
            returnClickToDeck();
        }, 2500);
    }

}

function cardHovering() {

    var card = $('.game-card');

    card.on('mouseenter', function () {
        clearInterval(animInt);

        if ($(this).hasClass('active')) {
            if ($(this).hasClass('rotate'))
                $(this).rotate({animateTo: 5.5, duration: 200});
        }
    });

    card.on('mouseleave', function () {
        clearInterval(animInt);
        animInt = setInterval(
                cardsReshuffle
                , 10000);
        if ($(this).hasClass('active')) {
            if ($(this).hasClass('rotate'))
                $(this).rotate({animateTo: 0, duration: 800});
        }
    });


}

function rnd() {
    return Math.floor(Math.random() * 52 + 1);
}

function getScore(id) {
    if (id <= 4) {
        return 11;
    } else if (id <= 8) {
        return 6;
    } else if (id <= 12) {
        return 7;
    } else if (id <= 16) {
        return 8;
    } else if (id <= 20) {
        return 9;
    } else if (id <= 36) {
        return 10;
    } else if (id <= 40) {
        return 2;
    } else if (id <= 44) {
        return 3;
    } else if (id <= 48) {
        return 4;
    } else if (id <= 52) {
        return 5;
    }
}

function reset() {

    $('.laid').animate({'left': '-100vw'});
    plScore.text(0);
    compScore.text(0);
    scoreComp = 0;
    scorePl = 0;
    leftpos = 0;
    angle = -10;
    passBtn.attr("disabled", "disabled");

}

function pass() {
    leftpos = 0;
    angle = -10;
    $(".game-card").off("click");
    $(".game-card").off("mouseenter");
    $(".game-card").off("mouseleave");
    passBtn.attr("disabled", "disabled");
    if (parseInt(compScore.text()) < parseInt(plScore.text())) {
        timer = setInterval(ai, 2000);
    }

}

function ai() {

    if ($(".game-card").length != 0) {
        clearInterval(animInt);
        let id = mass[cardNum];
        let $this = $('#' + mass[cardNum]);
        $this.removeClass("rotate");
        $this.rotate({animateTo: 0, duration: 800});
        leftpos = (leftpos == 0) ? leftpos += 4 : leftpos += 5;

        $this.animate({"left": leftpos + "%", "top": "4%", 'width': '159px', 'height': '228px'}, 1000);

        setTimeout(function () {
            $this.rotate({animateTo: angle += 4, duration: 800});
            $this.css("z-index", z++);
            $this.children('img').prop({"src": "cards/" + id + ".png"});
            $this.removeClass("game-card").addClass("laid");
            scoreComp += getScore(id);
            compScore.text(scoreComp);
            getLocalScore();
            cardNum--;
            $('#' + mass[cardNum]).addClass("active");
        }, 1000);
    } else {
        getLocalScore();
    }

}

function getGlobalScore() {
    if ($(".game-card").length == 0 || rounds == 11) {
        if ($('.comp-coin').length > $('.pl-coin').length) {
            $(".final").show('puff', 400);
            $(".final span").text("Game Over! Computer wins!");

        } else if ($('.comp-coin').length < $('.pl-coin').length) {
            $(".final").show('puff', 400);
            $(".final span").text("Congratulations! You Win!");
        } else if ($('.comp-coin').length == $('.pl-coin').length) {
            $(".final").show('puff', 400);
            $(".final span").text("Game Over! Draw!");
        }
    }

}

function getLocalScore() {
    if ($(".game-card").length != 0) {
        if (plScore.text() > 21) {
            compWins();
        } else if (plScore.text() == 21) {
            plWins();
        } else if (parseInt(compScore.text()) > parseInt(plScore.text())) {
            clearInterval(timer);
            if (parseInt(compScore.text()) > 21) {
                resultsTimeout(1000, "", plWins);
            } else {
                resultsTimeout(1000, "", compWins);
            }
        } else if (compScore.text() == 20 && compScore.text() == parseInt(plScore.text())) {
            clearInterval(timer);
            resultsTimeout(1000, "Draw!");
        } else if (compScore.text() <= 19 && compScore.text() >= 17 && compScore.text() == parseInt(plScore.text())) {
            let decision = Math.floor(Math.random() * 2);
            if (decision == 1) {
                clearInterval(timer);
                resultsTimeout(1000, "Draw!");
            }
        }
    } else {

        if (parseInt(compScore.text()) > parseInt(plScore.text())) {
            clearInterval(timer);
            resultsTimeout(1000, "", compWins);
        } else if (parseInt(compScore.text()) < parseInt(plScore.text())) {
            clearInterval(timer);
            resultsTimeout(1000, "", plWins);
        }
        setTimeout(getGlobalScore, 2000);
    }


}

function compWins() {

    alert("Sorry! You lost this round!");
    $('.gscore').first().animate({'top': '' + (coinStart += 18) + 'px'}, 500).addClass('comp-coin').removeClass('gscore');
    reset();
    if (rounds++ == 10) {
        $(".game-card").off("click");
        setTimeout(getGlobalScore, 1000);
        clearInterval(animInt);
    }
    getLocalScore();
}

function plWins() {
    window.alert("Congrats! You won this round!");
    $('.gscore').last().css("z-index", zIndCoin++);
    $('.gscore').last().animate({"top": "" + (coinEnd -= 18) + "px"}, 500).addClass('pl-coin').removeClass('gscore');
    reset();
    if (rounds++ == 10) {
        $(".game-card").off("click");
        setTimeout(getGlobalScore, 1000);
        clearInterval(animInt);
    }
    getLocalScore();
}

function resultsTimeout(time, msg, func) {
    setTimeout(function () {
        if (func != undefined)
            func();
        else {
            window.alert(msg);
            reset();
        }
        returnClickToDeck();
        cardHovering();
    }, time);
}