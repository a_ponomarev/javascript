function sumInput() {

    let arr = [];
    while (true) {

        let val = prompt("Enter a value: ");

        if (isNaN(val) || val=="" || val == null) 
            break;
        
        arr.push(+val);
        
    }

    let sum = 0;

    for (arrItem of arr) {

        sum += +arrItem;
    }

    return sum;

}

alert(sumInput());
