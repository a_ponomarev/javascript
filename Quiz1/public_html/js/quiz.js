function Book(author, title, publishDate, isEBook, price) {

    this.author = author;
    this.title = title;
    this.publishDate = publishDate;
    this.isEBook = isEBook;
    this.price = price;

    this.displayBookDetails = function () {

        for (key in this) {
            if (typeof this[key] == "string" || typeof this[key] == "number" || typeof this[key] == "boolean")
                console.log("key :" + key + ", Value: " + this[key]);

        }
    }


}

// only output the ebooks of the arrays

function displayStatus() {
    for (i of bookStore) {

        if (i["isEBook"] == true) {

            console.log("name: " + i["title"] + ", ebook: " + i["isEBook"]);
        }

    }

}

function addBook() {

    let name, title, year, ebook, price;

    name = prompt("Who's author?");
    title = prompt("What's the title?");
    year = prompt("What's the year?");
    ebook = confirm("Ebook?");
    price = +prompt("What's the price?");

    bookStore.push(new Book(name, title, year, ebook, price))
}

function adjustPrice(books, percent) {
    percent = percent / 100;
    for (i of books) {

        for (key in i) {
            if (key == "price") {

                let temp = i["price"] * percent;
                i["price"] += temp;
            }
        }

    }

}

function calculateAllPrices() {
    let sum = 0;
    for (i of bookStore) {

        for (key in i) {
            if (key == "price") {
                sum += i["price"];
            }
        }

    }
    return sum;

}

let bookStore = [];
bookStore[0] = new Book("Ahmad Kadir", "Future", 1999, false, 20);
bookStore[1] = new Book("Vladimir Putin", "Past", 2005, true, 10);
bookStore[2] = new Book("Justin Trudeau", "Canada", 2017, false, 30);

addBook();
console.log("The sum of all prices is:" + calculateAllPrices());
adjustPrice(bookStore, 10);
console.log("The sum of all prices (after adjustment) is:" + calculateAllPrices());
displayStatus();

console.log(" ");
console.log("******************");
console.log(" ");

for (i of bookStore) {

    i.displayBookDetails();
    console.log(" ");
    console.log("******************");
    console.log(" ");
}